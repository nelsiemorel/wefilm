import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
   //Déclaration variable
   movies = [];
   moviesList:any; 
   
   constructor(public http: HttpClient, public modalController: ModalController) {
     this.db('http://localhost/wefilm/src/php/getMovies.php').subscribe((data) => {
       console.log(data);
       this.movies= data[0];
       console.log(this.movies);
     });
     this.allMovies('http://localhost/wefilm/src/php/getMovies.php').subscribe( data => {
      this.moviesList = data; 
      }, err =>{
      console.log(err); 
      });
   }
   db(URL: string) {
     return this.http.get(URL);
   }
   allMovies(URL: string) {
    return this.http.get(URL);
  }
}
