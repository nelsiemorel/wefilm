import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage {
  client = {
    email :'',
    password:''
  }
  clients:any;
  clientsData:any;
  constructor(public http: HttpClient, public modalController: ModalController, public navCtrl : NavController, private router: Router) {

  }
    // Si c'est remplis
    validateInput(){
      let email = this.client.email.trim();
      let password = this.client.password.trim();
   
      return(this.client.email && this.client.password && email.length > 0 && password.length > 0 );
    }
  //méthode pour l'inscription
  insert() {
    if(this.validateInput()){
      this.dbClients('http://localhost/wefilm/src/php/getClients').subscribe( data => {
        this.clientsData = data;
      });
      this.Insertdata('http://localhost/wefilm/src/php/inscription',this.client).subscribe( data => {
        this.clients = data;
        this.router.navigate(['../connexion/']);
          console.log(this.client.email);
          console.log(this.client.password);
        });
          }
          else{
            alert("Votre mot de passe ou identifiant sont incorrectes");
          }
    }
    Insertdata(URL: string,client) {
      return this.http.post(URL,this.client,this.clients);
    }
    dbClients(URL: string) {
      return this.http.get(URL);
    }
  }

 
