import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PartieadminPageRoutingModule } from './partieadmin-routing.module';

import { PartieadminPage } from './partieadmin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PartieadminPageRoutingModule
  ],
  declarations: [PartieadminPage]
})
export class PartieadminPageModule {}
