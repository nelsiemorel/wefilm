import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { InscriptionPage } from '../inscription/inscription.page'
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage {

 //Déclaration variable
 clients:any;
 client = {
   email :'',
   password:''
 }
 constructor(public http: HttpClient, public modalController: ModalController, public navCtrl : NavController, private router: Router) {

 }
 //méthode pour l'inscription
 formInscription() {
   this.router.navigate(['../inscription/']);
 }
 // Si c'est remplis
 validateInput(){
   let email = this.client.email.trim();
   let password = this.client.password.trim();

   return(this.client.email && this.client.password && email.length > 0 && password.length > 0 );
 }
 //méthode pour prendre les données de la base de données avec une URL
 formLogindata(URL: string) {
   return this.http.get(URL);
 }
 formLogin() {
   if(this.validateInput()){
     this.formLogindata('http://localhost/wefilm/src/php/getClients').subscribe(async data => {
       this.clients = data;
       console.log(this.clients);
       for(let clientData of this.clients){
         console.log(clientData.email);
         console.log(this.client);
         if(clientData.email == this.client.email && clientData.password == this.client.password){
          alert("ok");
           this.router.navigate(['../tabs/']);
         }
         else{
           alert("Votre mot de passe ou identifiant sont incorrectes");
         }
       }
     }, err => {
       console.log(err);
     });
   }else{
     console.log("plus info");
   }
 }


}
