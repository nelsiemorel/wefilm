import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../api.service';
import { InscriptionPage } from '../inscription/inscription.page'
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage {
  admin = {
    pseudo:'',
    password:''
  };
  adminData = {
    pseudo:'admin',
    password:'admin'
  }
  constructor(public http: HttpClient, public modalController: ModalController, public navCtrl : NavController, private router: Router) { }
formLogin(){
  if(this.adminData.pseudo == this.admin.pseudo && this.adminData.password == this.admin.password){
    alert("ok");
     this.router.navigate(['../partieadmin/']);
   }
   else{
     alert("Votre mot de passe ou identifiant sont incorrectes");
   }
}

}
